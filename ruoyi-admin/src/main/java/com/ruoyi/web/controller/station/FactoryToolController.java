package com.ruoyi.web.controller.station;

import java.util.List;

import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.entity.SysRole;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.FactoryTool;
import com.ruoyi.system.service.IFactoryToolService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author ruoyi
 * @date 2021-01-18
 */
@Controller
@RequestMapping("/factory/factory")
public class FactoryToolController extends BaseController
{
    private String prefix = "factory/factory";

    @Autowired
    private IFactoryToolService factoryToolService;

    @RequiresPermissions("factory:factory:view")
    @GetMapping()
    public String tool()
    {
        return prefix + "/factory";
    }

    /**
     * 查询列表
     */
    @RequiresPermissions("system:post:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(FactoryTool factoryTool)
    {
        startPage();
        List<FactoryTool> list = factoryToolService.selectFactoryToolList(factoryTool);
        return getDataTable(list);
    }

    /**
     * 导出列表
     */
    @RequiresPermissions("system:post:export")
    @Log(title = "机器参数", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(FactoryTool factoryTool)
    {
        List<FactoryTool> list = factoryToolService.selectFactoryToolList(factoryTool);
        ExcelUtil<FactoryTool> util = new ExcelUtil<FactoryTool>(FactoryTool.class);
        return util.exportExcel(list, "factory");
    }

    /**
     * 新增
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存
     */
    @RequiresPermissions("system:post:add")
    @Log(title = "添加机器", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(FactoryTool factoryTool)
    {
        return toAjax(factoryToolService.insertFactoryTool(factoryTool));
    }

    /**
     * 修改
     */
    @GetMapping("/edit/{factoryId}")
    public String edit(@PathVariable("factoryId") Long factoryId, ModelMap mmap)
    {
        FactoryTool factoryTool = factoryToolService.selectFactoryToolById(factoryId);
        mmap.put("factoryTool", factoryTool);
        return prefix + "/edit";
    }

    /**
     * 修改保存
     */
    @RequiresPermissions("system:post:edit")
    @Log(title = "修改机器", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(FactoryTool factoryTool)
    {
        return toAjax(factoryToolService.updateFactoryTool(factoryTool));
    }

    /**
     * 删除
     */
    @RequiresPermissions("system:post:remove")
    @Log(title = "删除机器", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(factoryToolService.deleteFactoryToolByIds(ids));
    }
    /**
     * 校验部门名称
     */
    @PostMapping("/checkDeptNameUnique")
    @ResponseBody
    public String checkDeptNameUnique(FactoryTool factoryTool)
    {
        return factoryToolService.checkFactoryToolNameUnique (factoryTool);
    }

    /**
     * 选择部门树
     *
     * @param factoryId 部门ID
     * @param excludeId 排除ID
     */
    @GetMapping(value = { "/selectDeptTree/{factoryId}", "/selectDeptTree/{factoryId}/{excludeId}" })
    public String selectDeptTree(@PathVariable("factoryId") Long factoryId,
                                 @PathVariable(value = "excludeId", required = false) String excludeId, ModelMap mmap)
    {
        mmap.put("factoryTool", factoryToolService.selectFactoryToolById(factoryId));
        mmap.put("excludeId", excludeId);
        return prefix + "/tree";
    }

    /**
     * 加载部门列表树
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData()
    {
        List<Ztree> ztrees = factoryToolService.selectFactoryToolTree(new FactoryTool ());
        return ztrees;
    }

    /**
     * 加载部门列表树（排除下级）
     */
    @GetMapping("/treeData/{excludeId}")
    @ResponseBody
    public List<Ztree> treeDataExcludeChild(@PathVariable(value = "excludeId", required = false) Long excludeId)
    {
        FactoryTool factoryTool = new FactoryTool ();
        factoryTool.setFactoryId (excludeId);
        List<Ztree> ztrees = factoryToolService.selectTreeExcludeChild(factoryTool);
        return ztrees;
    }

    /**
     * 加载角色部门（数据权限）列表树
     */
    @GetMapping("/roleDeptTreeData")
    @ResponseBody
    public List<Ztree> deptTreeData(SysRole role)
    {
        List<Ztree> ztrees = factoryToolService.factoryToolTreeData (role);
        return ztrees;
    }
}
