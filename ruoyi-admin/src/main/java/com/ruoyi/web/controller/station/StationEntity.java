/*
package com.ruoyi.web.controller.station;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

*/
/**
 * fileName:StationEntity
 *   description:
 *   author: Mr.Ge
 *   createTime:2021/1/21 16:17
 *   version:1.0.0
 *//*


@ApiModel("工站实体")
class StationEntity {
	@ApiModelProperty("工站ID")
	private Integer stationId;

	@ApiModelProperty("工站名称")
	private String stationName;

	@ApiModelProperty("工站个数")
	private Integer num;

	@ApiModelProperty("工站图片")
	private String picUrl;

	@ApiModelProperty("工站顺序")
	private Integer orderNum;

	public Integer getStationId() {
		return stationId;
	}

	public void setStationId(Integer stationId) {
		this.stationId = stationId;
	}

	public String getStationName() {
		return stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public Integer getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}

	public StationEntity() {
		super();
	}

	public StationEntity(Integer stationId, String stationName, Integer num, String picUrl, Integer orderNum) {
		super();
		this.stationId = stationId;
		this.stationName = stationName;
		this.num = num;
		this.picUrl = picUrl;
		this.orderNum = orderNum;
	}

}
*/
