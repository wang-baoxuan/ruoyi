package com.ruoyi.web.controller.demo.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.PageDomain;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.page.TableSupport;

/**
 * 工站相关
 * 
 * @author ruoyi
 */
@Controller
@RequestMapping("/demo/station")
public class DemoStationController extends BaseController {
	private String prefix = "demo/station";

	private final static List<StationTableModel> station = new ArrayList<StationTableModel>();
	{
		station.add(new StationTableModel(1, "2020/9/6", "7:01:36", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"));
		station.add(new StationTableModel(1, "2020/9/6", "7:01:38", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"));
		station.add(new StationTableModel(1, "2020/9/6", "7:01:40", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"));
		station.add(new StationTableModel(1, "2020/9/6", "7:01:42", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"));
		station.add(new StationTableModel(1, "2020/9/6", "7:01:44", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"));
		station.add(new StationTableModel(1, "2020/9/6", "7:01:46", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"));
		station.add(new StationTableModel(1, "2020/9/6", "7:01:48", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"));
		station.add(new StationTableModel(1, "2020/9/6", "7:01:50", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"));
		station.add(new StationTableModel(1, "2020/9/6", "7:01:52", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"));
		station.add(new StationTableModel(1, "2020/9/6", "7:01:54", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"));
		station.add(new StationTableModel(1, "2020/9/6", "7:01:56", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"));
		station.add(new StationTableModel(1, "2020/9/6", "7:01:58", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"));
	}

	private final static List<UserTableColumn> columns = new ArrayList<UserTableColumn>();
	{
		columns.add(new UserTableColumn("用户ID", "userId"));
		columns.add(new UserTableColumn("用户编号", "userCode"));
		columns.add(new UserTableColumn("用户姓名", "userName"));
		columns.add(new UserTableColumn("用户手机", "userPhone"));
		columns.add(new UserTableColumn("用户邮箱", "userEmail"));
		columns.add(new UserTableColumn("用户状态", "status"));
	}

	/**
	 * 首页
	 */
	@GetMapping("/index")
	public String index() {
		return prefix + "/index";
	}

	/**
	 * 搜索相关
	 */
	@GetMapping("/search")
	public String search() {
		return prefix + "/search";
	}

	/**
	 * 数据汇总
	 */
	@GetMapping("/footer")
	public String footer() {
		return prefix + "/footer";
	}

	/**
	 * 组合表头
	 */
	@GetMapping("/groupHeader")
	public String groupHeader() {
		return prefix + "/groupHeader";
	}

	/**
	 * 表格导出
	 */
	@GetMapping("/export")
	public String export() {
		return prefix + "/export";
	}

	/**
	 * 翻页记住选择
	 */
	@GetMapping("/remember")
	public String remember() {
		return prefix + "/remember";
	}

	/**
	 * 跳转至指定页
	 */
	@GetMapping("/pageGo")
	public String pageGo() {
		return prefix + "/pageGo";
	}

	/**
	 * 自定义查询参数
	 */
	@GetMapping("/params")
	public String params() {
		return prefix + "/params";
	}

	/**
	 * 多表格
	 */
	@GetMapping("/multi")
	public String multi() {
		return prefix + "/multi";
	}

	/**
	 * 点击按钮加载表格
	 */
	@GetMapping("/button")
	public String button() {
		return prefix + "/button";
	}

	/**
	 * 直接加载表格数据
	 */
	@GetMapping("/data")
	public String data(ModelMap mmap) {
		mmap.put("users", station);
		return prefix + "/data";
	}

	/**
	 * 表格冻结列
	 */
	@GetMapping("/fixedColumns")
	public String fixedColumns() {
		return prefix + "/fixedColumns";
	}

	/**
	 * 自定义触发事件
	 */
	@GetMapping("/event")
	public String event() {
		return prefix + "/event";
	}

	/**
	 * 表格细节视图
	 */
	@GetMapping("/detail")
	public String detail() {
		return prefix + "/detail";
	}

	/**
	 * 表格父子视图
	 */
	@GetMapping("/child")
	public String child() {
		return prefix + "/child";
	}

	/**
	 * 表格图片预览
	 */
	@GetMapping("/image")
	public String image() {
		return prefix + "/image";
	}

	/**
	 * 动态增删改查
	 */
	@GetMapping("/curd")
	public String curd() {
		return prefix + "/curd";
	}

	/**
	 * 表格拖拽操作
	 */
	@GetMapping("/reorder")
	public String reorder() {
		return prefix + "/reorder";
	}

	/**
	 * 表格列宽拖动
	 */
	@GetMapping("/resizable")
	public String resizable() {
		return prefix + "/resizable";
	}

	/**
	 * 表格行内编辑操作
	 */
	@GetMapping("/editable")
	public String editable() {
		return prefix + "/editable";
	}

	/**
	 * 主子表提交
	 */
	@GetMapping("/subdata")
	public String subdata() {
		return prefix + "/subdata";
	}

	/**
	 * 表格自动刷新
	 */
	@GetMapping("/refresh")
	public String refresh() {
		return prefix + "/refresh";
	}

	/**
	 * 表格打印配置
	 */
	@GetMapping("/print")
	public String print() {
		return prefix + "/print";
	}

	/**
	 * 表格标题格式化
	 */
	@GetMapping("/headerStyle")
	public String headerStyle() {
		return prefix + "/headerStyle";
	}

	/**
	 * 表格动态列
	 */
	@GetMapping("/dynamicColumns")
	public String dynamicColumns() {
		return prefix + "/dynamicColumns";
	}

	/**
	 * 表格其他操作
	 */
	@GetMapping("/other")
	public String other() {
		return prefix + "/other";
	}

	/**
	 * 动态获取列
	 */
	@PostMapping("/ajaxColumns")
	@ResponseBody
	public AjaxResult ajaxColumns(UserTableColumn userColumn) {
		List<UserTableColumn> columnList = new ArrayList<UserTableColumn>(
				Arrays.asList(new UserTableColumn[columns.size()]));
		Collections.copy(columnList, columns);
		if (userColumn != null && "userBalance".equals(userColumn.getField())) {
			columnList.add(new UserTableColumn("用户余额", "userBalance"));
		}
		return AjaxResult.success(columnList);
	}

	/**
	 * 查询数据
	 */
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(UserTableModel userModel) {
		TableDataInfo rspData = new TableDataInfo();
		List<StationTableModel> userList = new ArrayList<StationTableModel>(
				Arrays.asList(new StationTableModel[station.size()]));
		Collections.copy(userList, station);
		PageDomain pageDomain = TableSupport.buildPageRequest();
		if (null == pageDomain.getPageNum() || null == pageDomain.getPageSize()) {
			rspData.setRows(userList);
			rspData.setTotal(userList.size());
			return rspData;
		}
		Integer pageNum = (pageDomain.getPageNum() - 1) * 10;
		Integer pageSize = pageDomain.getPageNum() * 10;
		if (pageSize > userList.size()) {
			pageSize = userList.size();
		}
		rspData.setRows(userList.subList(pageNum, pageSize));
		rspData.setTotal(userList.size());
		return rspData;
	}
}

class StationTableModel {
	private int id;
	private String date;
	private String time;
	private String B2601;
	private String B2602;
	private String B2603;
	private String B2604;
	private String B2605;
	private String B2606;
	private String B2607;
	private String B2608;
	private String B2609;
	private String B2610;
	private String B2611;
	private String B2612;
	private String B2613;
	private String B2614;
	private String B2615;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getB2601() {
		return B2601;
	}

	public void setB2601(String b2601) {
		B2601 = b2601;
	}

	public String getB2602() {
		return B2602;
	}

	public void setB2602(String b2602) {
		B2602 = b2602;
	}

	public String getB2603() {
		return B2603;
	}

	public void setB2603(String b2603) {
		B2603 = b2603;
	}

	public String getB2604() {
		return B2604;
	}

	public void setB2604(String b2604) {
		B2604 = b2604;
	}

	public String getB2605() {
		return B2605;
	}

	public void setB2605(String b2605) {
		B2605 = b2605;
	}

	public String getB2606() {
		return B2606;
	}

	public void setB2606(String b2606) {
		B2606 = b2606;
	}

	public String getB2607() {
		return B2607;
	}

	public void setB2607(String b2607) {
		B2607 = b2607;
	}

	public String getB2608() {
		return B2608;
	}

	public void setB2608(String b2608) {
		B2608 = b2608;
	}

	public String getB2609() {
		return B2609;
	}

	public void setB2609(String b2609) {
		B2609 = b2609;
	}

	public String getB2610() {
		return B2610;
	}

	public void setB2610(String b2610) {
		B2610 = b2610;
	}

	public String getB2611() {
		return B2611;
	}

	public void setB2611(String b2611) {
		B2611 = b2611;
	}

	public String getB2612() {
		return B2612;
	}

	public void setB2612(String b2612) {
		B2612 = b2612;
	}

	public String getB2613() {
		return B2613;
	}

	public void setB2613(String b2613) {
		B2613 = b2613;
	}

	public String getB2614() {
		return B2614;
	}

	public void setB2614(String b2614) {
		B2614 = b2614;
	}

	public String getB2615() {
		return B2615;
	}

	public void setB2615(String b2615) {
		B2615 = b2615;
	}

	public StationTableModel() {
		super();
	}

	public StationTableModel(int id, String date, String time, String b2601, String b2602, String b2603, String b2604,
			String b2605, String b2606, String b2607, String b2608, String b2609, String b2610, String b2611,
			String b2612, String b2613, String b2614, String b2615) {
		super();
		this.id = id;
		this.date = date;
		this.time = time;
		B2601 = b2601;
		B2602 = b2602;
		B2603 = b2603;
		B2604 = b2604;
		B2605 = b2605;
		B2606 = b2606;
		B2607 = b2607;
		B2608 = b2608;
		B2609 = b2609;
		B2610 = b2610;
		B2611 = b2611;
		B2612 = b2612;
		B2613 = b2613;
		B2614 = b2614;
		B2615 = b2615;
	}

}