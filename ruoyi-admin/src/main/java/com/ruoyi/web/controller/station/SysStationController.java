package com.ruoyi.web.controller.station;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.core.domain.entity.SysRole;
import com.ruoyi.common.core.domain.entity.SysStation;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ShiroUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.FactoryTool;
import com.ruoyi.system.service.ISysStationService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * fileName:Stationcontroller
 * description:
 * author: Mr.Ge
 * createTime:2021/1/21 16:29
 * version:1.0.0
 */
@Controller
@RequestMapping("/factory/station")
public class SysStationController extends BaseController{

    private String prefix = "factory/station";

    @Autowired
    private ISysStationService stationService;

    @RequiresPermissions("factory:station:view")
    @GetMapping()
    public String station()
    {
        return prefix + "/station";
    }

    @RequiresPermissions("factory:station:list")
    @PostMapping("/list")
    @ResponseBody
    public List<SysStation> list(SysStation station)
    {
        List<SysStation> stationList = stationService.selectStationList(station);
        return stationList;
    }

    /**
     * 新增分类
     */
    @GetMapping("/add/{parentId}")
    public String add(@PathVariable("parentId") Long parentId, ModelMap mmap)
    {
        mmap.put("station", stationService.selectStationById(parentId));
        return prefix + "/add";
    }

    /**
     * 新增保存分类
     */
    @Log(title = "分类管理", businessType = BusinessType.INSERT)
    @RequiresPermissions("factory:station:add")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@Validated SysStation station)
    {
        if (UserConstants.DEPT_NAME_NOT_UNIQUE.equals(stationService.checkStationNameUnique(station)))
        {
            return error("新增分类'" + station.getStationName() + "'失败，分类名称已存在");
        }
        station.setCreateBy(ShiroUtils.getLoginName());
        return toAjax(stationService.insertStation(station));
    }

    /**
     * 修改
     */
    @GetMapping("/edit/{stationId}")
    public String edit(@PathVariable("stationId") Long stationId, ModelMap mmap)
    {
        SysStation station = stationService.selectStationById(stationId);
        if (StringUtils.isNotNull(station) && 100L == stationId)
        {
            station.setParentName("无");
        }
        mmap.put("station", station);
        return prefix + "/edit";
    }

    /**
     * 保存
     */
    @Log(title = "分类管理", businessType = BusinessType.UPDATE)
    @RequiresPermissions("factory:station:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@Validated SysStation station)
    {
        if (UserConstants.DEPT_NAME_NOT_UNIQUE.equals(stationService.checkStationNameUnique(station)))
        {
            return error("修改分类'" + station.getStationName() + "'失败，分类名称已存在");
        }
        else if (station.getParentId().equals(station.getStationId()))
        {
            return error("修改分类'" + station.getStationName() + "'失败，上级分类不能是自己");
        }
        else if (StringUtils.equals(UserConstants.DEPT_DISABLE, station.getStatus())
                && stationService.selectNormalChildrenStationById(station.getStationId()) > 0)
        {
            return AjaxResult.error("该分类包含未停用的子分类！");
        }
        station.setUpdateBy(ShiroUtils.getLoginName());
        return toAjax(stationService.updateStation(station));
    }

    /**
     * 删除
     */
    @Log(title = "分类管理", businessType = BusinessType.DELETE)
    @RequiresPermissions("factory:station:remove")
    @GetMapping("/remove/{stationId}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("stationId") Long stationId)
    {
        if (stationService.selectStationCount(stationId) > 0)
        {
            return AjaxResult.warn("存在下级分类,不允许删除");
        }

        return toAjax(stationService.deleteStationById(stationId));
    }

    /**
     * 校验分类名称
     */
    @PostMapping("/checkStationNameUnique")
    @ResponseBody
    public String checkStationNameUnique(SysStation station)
    {
        return stationService.checkStationNameUnique(station);
    }

    /**
     * 选择分类树
     *
     * @param stationId 分类ID
     * @param excludeId 排除ID
     */
    @GetMapping(value = { "/selectStationTree/{stationId}", "/selectStationTree/{stationId}/{excludeId}" })
    public String selectStationTree(@PathVariable("stationId") Long stationId,
                                 @PathVariable(value = "excludeId", required = false) String excludeId, ModelMap mmap)
    {
        mmap.put("station", stationService.selectStationById(stationId));
        mmap.put("excludeId", excludeId);
        return prefix + "/tree";
    }

    /**
     * 加载分类列表树
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData()
    {
        List<Ztree> ztrees = stationService.selectStationTree(new SysStation());
        return ztrees;
    }

    /**
     * 加载分类列表树（排除下级）
     */
    @GetMapping("/treeData/{excludeId}")
    @ResponseBody
    public List<Ztree> treeDataExcludeChild(@PathVariable(value = "excludeId", required = false) Long excludeId)
    {
        SysStation station = new SysStation();
        station.setStationId(excludeId);
        List<Ztree> ztrees = stationService.selectStationTreeExcludeChild(station);
        return ztrees;
    }

    /**
     * 加载角色分类（数据权限）列表树
     */
    @GetMapping("/roleStationTreeData")
    @ResponseBody
    public List<Ztree> stationTreeData(SysRole role)
    {
        List<Ztree> ztrees = stationService.roleStationTreeData(role);
        return ztrees;
    }
}
