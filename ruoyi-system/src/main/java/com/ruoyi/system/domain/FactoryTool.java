package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 对象 factory_tool
 * 
 * @author ruoyi
 * @date 2021-01-19
 */
public class FactoryTool extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long factoryId;

    /** 父节点id */
    @Excel(name = "父节点id")
    private Long parentId;

    /** 祖级列表 */
    @Excel(name = "祖级列表")
    private String ancestors;

    /** 名称 */
    @Excel(name = "名称")
    private String factoryName;

    /** 进料容量 */
    @Excel(name = "进料容量")
    private String enterCapacity;

    /** 出料容量 */
    @Excel(name = "出料容量")
    private String outputCapacity;

    /** 显示顺序 */
    @Excel(name = "显示顺序")
    private Long sequenceId;

    /** 图片 */
    @Excel(name = "图片")
    private String factoryImg;

    /** 分类状态（0正常 1停用） */
    @Excel(name = "分类状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setFactoryId(Long factoryId) 
    {
        this.factoryId = factoryId;
    }

    public Long getFactoryId() 
    {
        return factoryId;
    }
    public void setParentId(Long parentId) 
    {
        this.parentId = parentId;
    }

    public Long getParentId() 
    {
        return parentId;
    }
    public void setAncestors(String ancestors) 
    {
        this.ancestors = ancestors;
    }

    public String getAncestors() 
    {
        return ancestors;
    }
    public void setFactoryName(String factoryName) 
    {
        this.factoryName = factoryName;
    }

    public String getFactoryName() 
    {
        return factoryName;
    }
    public void setEnterCapacity(String enterCapacity) 
    {
        this.enterCapacity = enterCapacity;
    }

    public String getEnterCapacity() 
    {
        return enterCapacity;
    }
    public void setOutputCapacity(String outputCapacity) 
    {
        this.outputCapacity = outputCapacity;
    }

    public String getOutputCapacity() 
    {
        return outputCapacity;
    }
    public void setSequenceId(Long sequenceId) 
    {
        this.sequenceId = sequenceId;
    }

    public Long getSequenceId() 
    {
        return sequenceId;
    }
    public void setFactoryImg(String factoryImg) 
    {
        this.factoryImg = factoryImg;
    }

    public String getFactoryImg() 
    {
        return factoryImg;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("factoryId", getFactoryId())
            .append("parentId", getParentId())
            .append("ancestors", getAncestors())
            .append("factoryName", getFactoryName())
            .append("enterCapacity", getEnterCapacity())
            .append("outputCapacity", getOutputCapacity())
            .append("sequenceId", getSequenceId())
            .append("factoryImg", getFactoryImg())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
