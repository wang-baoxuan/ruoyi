package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.entity.SysRole;
import com.ruoyi.system.domain.FactoryTool;
import org.apache.ibatis.annotations.Param;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2021-01-18
 */
public interface IFactoryToolService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param factoryId 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public FactoryTool selectFactoryToolById(Long factoryId);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param factoryTool 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<FactoryTool> selectFactoryToolList(FactoryTool factoryTool);

    /**
     * 新增【请填写功能名称】
     * 
     * @param factoryTool 【请填写功能名称】
     * @return 结果
     */
    public int insertFactoryTool(FactoryTool factoryTool);

    /**
     * 修改【请填写功能名称】
     * 
     * @param factoryTool 【请填写功能名称】
     * @return 结果
     */
    public int updateFactoryTool(FactoryTool factoryTool);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFactoryToolByIds(String ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param factoryId 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteFactoryToolById(Long factoryId);

    /**
     * 查询机器总台数
     * @param parentId 父id
     * @return
     */
    public int selectFactoryToolCount(Long parentId);




    /**
     * 校验分类名称是否唯一
     * @param factoryTool
     * @return
     */
    public String  checkFactoryToolNameUnique(FactoryTool factoryTool);


    /**
     * 修改所在分类的父级分类状态
     *
     * @param factoryTool
     */
    public void updateFactoryToolStatus(FactoryTool factoryTool);

    /**
     * 根据id查询所有的子分类
     * @param factoryId
     * @return
     */
    public List<FactoryTool> selectChildrenById(Long factoryId);

    /**
     * 根据Id查询所有子分类数量(正常状态)
     * @param factoryId
     * @return
     */
    public int selectNormalChildrenById(Long factoryId);

    /**
     * 查询分类管理树
     *
     * @param factoryTool 分类信息
     * @return 所有分类信息
     */
    public List<Ztree> selectFactoryToolTree(FactoryTool factoryTool);

    /**
     * 查询分类管理树（排除下级）
     *
     * @param factoryTool 分类信息
     * @return 所有分类信息
     */
    public List<Ztree> selectTreeExcludeChild(FactoryTool factoryTool);

    /**
     * 根据角色ID查询分类（数据权限）
     *
     * @param role 角色对象
     * @return 分类列表（数据权限）
     */
    public List<Ztree> factoryToolTreeData(SysRole role);

    /**
     * 查询分类管理树
     *
     * @param factoryTool 分类信息
     * @return 所有分类信息
     */
    public List<Ztree> selectFactoryTree(FactoryTool factoryTool);

}
