package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.entity.SysRole;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.FactoryTool;
import com.ruoyi.system.mapper.FactoryToolMapper;
import com.ruoyi.system.service.IFactoryToolService;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.core.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-01-18
 */
@Service
public class FactoryToolServiceImpl implements IFactoryToolService
{
    @Autowired
    private FactoryToolMapper factoryToolMapper;
    /**
     * 查询列表
     *
     * @param factoryTool 分类
     * @return 分类集合
     */
    @Override
    @DataScope(stationAlias = "d")
    public List<FactoryTool> selectFactoryToolList(FactoryTool factoryTool)
    {
        return factoryToolMapper.selectFactoryToolList(factoryTool);
    }

    /**
     * 查询分类管理树
     *
     * @param factoryTool 分类信息
     * @return 所有分类信息
     */
    @Override
    @DataScope(stationAlias = "d")
    public List<Ztree> selectFactoryToolTree(FactoryTool factoryTool) {
        List<FactoryTool> factoryToolList = factoryToolMapper.selectFactoryToolList (factoryTool);
        List<Ztree> ztrees = initZtree (factoryToolList);
        return ztrees;
    }

    /**
     * 查询分类管理树（排除下级）
     *
     * @param factoryTool 分类信息
     * @return 所有分类信息
     */
    @Override
    @DataScope(stationAlias = "d")
    public List<Ztree> selectTreeExcludeChild(FactoryTool factoryTool) {
        Long factoryId = factoryTool.getFactoryId ();
        List<FactoryTool> factoryToolList = factoryToolMapper.selectFactoryToolList (factoryTool);
        Iterator<FactoryTool> it = factoryToolList.iterator();
        while (it.hasNext())
        {
            FactoryTool f = it.next();
            if (f.getFactoryId ().intValue() == factoryId
                    || ArrayUtils.contains(StringUtils.split(f.getAncestors(), ","), factoryId + ""))
            {
                it.remove();
            }
        }
        List<Ztree> ztrees = initZtree(factoryToolList);
        return ztrees;
    }
    /**
     * 根据角色ID查询分类（数据权限）
     *
     * @param role 角色对象
     * @return 部门列表（数据权限）
     */
    @Override
    public List<Ztree> factoryToolTreeData(SysRole role) {
        Long roleId = role.getRoleId();
        List<Ztree> ztrees = new ArrayList<Ztree>();
        List<FactoryTool> factoryToolList = selectFactoryToolList (new FactoryTool ());
        if (StringUtils.isNotNull(roleId))
        {
            List<String> factoryTree = factoryToolMapper.selectRoleFactoryTree(roleId);
            ztrees = initZtree(factoryToolList, factoryTree);
        }
        else
        {
            ztrees = initZtree(factoryToolList);
        }
        return ztrees;
    }

    /**
     * 对象转部门树
     *
     * @param factoryToolList 部门列表
     * @return 树结构列表
     */
    public List<Ztree> initZtree(List<FactoryTool> factoryToolList)
    {
        return initZtree(factoryToolList, null);
    }
    /**
     * 对象转部门树
     *
     * @param factoryToolList 部门列表
     * @param roleFactoryList 角色已存在菜单列表
     * @return 树结构列表
     */
    public List<Ztree> initZtree(List<FactoryTool> factoryToolList, List<String> roleFactoryList)
    {

        List<Ztree> ztrees = new ArrayList<Ztree>();
        boolean isCheck = StringUtils.isNotNull(roleFactoryList);
        for (FactoryTool factoryTool : factoryToolList)
        {
            if (UserConstants.DEPT_NORMAL.equals(factoryTool.getStatus()))
            {
                Ztree ztree = new Ztree();
                ztree.setId(factoryTool.getFactoryId ());
                ztree.setpId(factoryTool.getParentId());
                ztree.setName(factoryTool.getFactoryName ());
                ztree.setTitle(factoryTool.getFactoryName ());
                if (isCheck)
                {
                    ztree.setChecked(roleFactoryList.contains(factoryTool.getFactoryId () + factoryTool.getFactoryName ()));
                }
                ztrees.add(ztree);
            }
        }
        return ztrees;
    }
    /**
     * 查询机器总台数
     * @param parentId
     * @return
     */
    @Override
    public int selectFactoryToolCount(Long parentId) {
        FactoryTool factoryTool = new FactoryTool ();
        factoryTool.setParentId(parentId);
        return factoryToolMapper.selectFactoryToolCount(factoryTool);
    }

    /**
     * 删除信息
     *
     * @param factoryId ID
     * @return 结果
     */
    @Override
    public int deleteFactoryToolById(Long factoryId)
    {
        return factoryToolMapper.deleteFactoryToolById(factoryId);
    }

    /**
     * 查询
     * 
     * @param factoryId ID
     * @return
     */
    @Override
    public FactoryTool selectFactoryToolById(Long factoryId)
    {
        return factoryToolMapper.selectFactoryToolById(factoryId);
    }



    /**
     * 新增【请填写功能名称】
     * 
     * @param factoryTool 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertFactoryTool(FactoryTool factoryTool)
    {
        return factoryToolMapper.insertFactoryTool(factoryTool);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param factoryTool 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateFactoryTool(FactoryTool factoryTool)
    {
        return factoryToolMapper.updateFactoryTool(factoryTool);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteFactoryToolByIds(String ids)
    {
        return factoryToolMapper.deleteFactoryToolByIds(Convert.toStrArray(ids));
    }


    /**
     *  修改子元素关系
     * @param
     * @return
     */

    public int updateChildren(Long factoryId, String newAncestors, String oldAncestors) {
        List<FactoryTool> children = factoryToolMapper.selectChildrenById (factoryId);
        for (FactoryTool child : children)
        {
            child.setAncestors(child.getAncestors().replace(oldAncestors, newAncestors));
        }
        if (children.size() > 0)
        {
            factoryToolMapper.updateChildren (children);
        }
        return 0;
    }
    /**
     * 校验分类名称是否唯一
     * @param factoryTool
     * @return
     */
    @Override
    public String checkFactoryToolNameUnique(FactoryTool factoryTool) {
        Long factoryId = StringUtils.isNull(factoryTool.getFactoryId ()) ? -1L : factoryTool.getFactoryId ();
        FactoryTool info = factoryToolMapper.checkFactoryToolNameUnique (factoryTool.getFactoryName (),factoryTool.getParentId ());
        if (StringUtils.isNotNull(info) && info.getFactoryId ().longValue() != factoryId.longValue())
        {
            return UserConstants.DEPT_NAME_NOT_UNIQUE;
        }
        return UserConstants.DEPT_NAME_UNIQUE;
    }
    /**
     * 修改所在分类的父级分类状态
     *
     * @param factoryTool
     */
    @Override
    public void updateFactoryToolStatus(FactoryTool factoryTool) {
        factoryTool = factoryToolMapper.selectFactoryToolById (factoryTool.getFactoryId ());
        factoryToolMapper.updateFactoryToolStatus (factoryTool);
    }
    /**
     * 根据id查询所有的子分类
     * @param factoryId
     * @return
     */
    @Override
    public List<FactoryTool> selectChildrenById(Long factoryId) {
        return factoryToolMapper.selectChildrenById (factoryId);
    }
    /**
     * 根据Id查询所有子分类数量(正常状态)
     * @param factoryId
     * @return
     */
    @Override
    public int selectNormalChildrenById(Long factoryId) {
        return factoryToolMapper.selectNormalChildrenById (factoryId);
    }



    /**
     * 查询部门管理树
     *
     * @param factoryTool 部门信息
     * @return 所有部门信息
     */
    @Override
    public List<Ztree> selectFactoryTree(FactoryTool factoryTool) {
        List<FactoryTool> factoryToolList = factoryToolMapper.selectFactoryToolList(factoryTool);
        List<Ztree> ztrees = initZtree(factoryToolList);
        return ztrees;
    }




}
