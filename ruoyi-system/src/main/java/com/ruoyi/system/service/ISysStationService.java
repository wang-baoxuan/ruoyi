package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.core.domain.entity.SysStation;
import com.ruoyi.common.core.domain.entity.SysRole;

import java.util.List;

/**
 * 分类管理 服务层
 * 
 * @author ruoyi
 */
public interface ISysStationService
{
    /**
     * 查询分类管理数据
     * 
     * @param station 分类信息
     * @return 分类信息集合
     */
    public List<SysStation> selectStationList(SysStation station);

    /**
     * 查询分类管理树
     * 
     * @param station 分类信息
     * @return 所有分类信息
     */
    public List<Ztree> selectStationTree(SysStation station);

    /**
     * 查询分类管理树（排除下级）
     * 
     * @param station 分类信息
     * @return 所有分类信息
     */
    public List<Ztree> selectStationTreeExcludeChild(SysStation station);

    /**
     * 根据角色ID查询菜单
     *
     * @param role 角色对象
     * @return 菜单列表
     */
    public List<Ztree> roleStationTreeData(SysRole role);

    /**
     * 查询机器数量
     * 
     * @param parentId 父分类ID
     * @return 结果
     */
    public int selectStationCount(Long parentId);


    /**
     * 删除分类管理信息
     * 
     * @param stationId 分类ID
     * @return 结果
     */
    public int deleteStationById(Long stationId);

    /**
     * 新增保存分类信息
     * 
     * @param station 分类信息
     * @return 结果
     */
    public int insertStation(SysStation station);

    /**
     * 修改保存分类信息
     * 
     * @param station 分类信息
     * @return 结果
     */
    public int updateStation(SysStation station);

    /**
     * 根据分类ID查询信息
     * 
     * @param stationId 分类ID
     * @return 分类信息
     */
    public SysStation selectStationById(Long stationId);

    /**
     * 根据ID查询所有子分类（正常状态）
     * 
     * @param stationId 分类ID
     * @return 子分类数
     */
    public int selectNormalChildrenStationById(Long stationId);

    /**
     * 校验分类名称是否唯一
     * 
     * @param station 分类信息
     * @return 结果
     */
    public String checkStationNameUnique(SysStation station);
}
