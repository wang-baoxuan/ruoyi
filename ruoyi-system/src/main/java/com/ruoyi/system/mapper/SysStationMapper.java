package com.ruoyi.system.mapper;

import com.ruoyi.common.core.domain.entity.SysStation;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 工站管理 数据层
 * 
 * @author ruoyi
 */
public interface SysStationMapper
{
    /**
     * 查询机械数量
     * 
     * @param station 信息
     * @return 结果
     */
    public int selectStationCount(SysStation station);

    /**
     * 查询分类管理数据
     * 
     * @param station 信息
     * @return 信息集合
     */
    public List<SysStation> selectStationList(SysStation station);

    /**
     * 删除分类管理信息
     * 
     * @param stationId ID
     * @return 结果
     */
    public int deleteStationById(Long stationId);

    /**
     * 新增分类信息
     * 
     * @param station 信息
     * @return 结果
     */
    public int insertStation(SysStation station);

    /**
     * 修改分类信息
     * 
     * @param station 信息
     * @return 结果
     */
    public int updateStation(SysStation station);

    /**
     * 修改子元素关系
     * 
     * @param stations 子元素
     * @return 结果
     */
    public int updateStationChildren(@Param("stations") List<SysStation> stations);

    /**
     * 根据分类ID查询信息
     * 
     * @param stationId ID
     * @return 信息
     */
    public SysStation selectStationById(Long stationId);

    /**
     * 校验机器名称是否唯一
     * 
     * @param stationName 名称
     * @param parentId 父ID
     * @return 结果
     */
    public SysStation checkStationNameUnique(@Param("stationName") String stationName, @Param("parentId") Long parentId);

    /**
     * 根据角色ID查询分类
     *
     * @param roleId 角色ID
     * @return 列表
     */
    public List<String> selectRoleStationTree(Long roleId);

    /**
     * 修改所在分类的父级状态
     * 
     * @param station
     */
    public void updateStationStatus(SysStation station);

    /**
     * 根据ID查询所有子分类
     * 
     * @param stationId ID
     * @return 列表
     */
    public List<SysStation> selectChildrenStationById(Long stationId);

    /**
     * 根据ID查询所有子（正常状态）
     * 
     * @param stationId ID
     * @return 子分类数
     */
    public int selectNormalChildrenStationById(Long stationId);
}
