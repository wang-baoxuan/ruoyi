package com.ruoyi.system.mapper;

import java.util.List;

import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.system.domain.FactoryTool;
import org.apache.ibatis.annotations.Param;

/**
 * Mapper接口
 * 
 * @author ruoyi
 * @date 2021-01-18
 */
public interface FactoryToolMapper 
{
    /**
     * 查询
     * 
     * @param factoryId
     * @return
     */
    public FactoryTool selectFactoryToolById(Long factoryId);

    /**
     * 查询列表
     * 
     * @param factoryTool
     * @return 集合
     */
    public List<FactoryTool> selectFactoryToolList(FactoryTool factoryTool);

    /**
     * 新增
     * 
     * @param factoryTool
     * @return 结果
     */
    public int insertFactoryTool(FactoryTool factoryTool);

    /**
     * 修改
     *
     * @param
     * @return 结果
     */
    public int updateFactoryTool(FactoryTool factoryTool);

    /**
     * 删除
     *
     * @param factoryId ID
     * @return 结果
     */
    public int deleteFactoryToolById(Long factoryId);

    /**
     * 批量删除
     * 
     * @param factoryIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteFactoryToolByIds(String[] factoryIds);

    /**
     * 查询机器总台数
     * @param factoryTool
     * @return
     */
    public int selectFactoryToolCount(FactoryTool factoryTool);



    /**
     *  修改子元素关系
     * @param factoryTools
     * @return
     */
    public int updateChildren(@Param("factoryTools") List<FactoryTool> factoryTools);

    /**
     * 校验分类名称是否唯一
     * @param factoryName
     * @param parentId
     * @return
     */
    public FactoryTool  checkFactoryToolNameUnique(@Param("factoryName") String factoryName, @Param("parentId") Long parentId);


    /**
     * 修改所在分类的父级分类状态
     *
     * @param factoryTool
     */
    public void updateFactoryToolStatus(FactoryTool factoryTool);

    /**
     * 根据id查询所有的子分类
     * @param factoryId
     * @return
     */
    public List<FactoryTool> selectChildrenById(Long factoryId);

    /**
     * 根据Id查询所有子分类数量(正常状态)
     * @param factoryId
     * @return
     */
    public int selectNormalChildrenById(Long factoryId);
    /**
     * 根据角色ID查询分类
     *
     * @param roleId 角色ID
     * @return 分类列表
     */
    public List<String> selectRoleFactoryTree(Long roleId);
}
