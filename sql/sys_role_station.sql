/*
Navicat MySQL Data Transfer

Source Server         : dcjh
Source Server Version : 80011
Source Host           : localhost:3306
Source Database       : jh_db

Target Server Type    : MYSQL
Target Server Version : 80011
File Encoding         : 65001

Date: 2021-01-21 21:12:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sys_role_station`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_station`;
CREATE TABLE `sys_role_station` (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `station_id` bigint(20) NOT NULL COMMENT '閮ㄩ棬ID',
  PRIMARY KEY (`role_id`,`station_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色和部门关联表';

-- ----------------------------
-- Records of sys_role_station
-- ----------------------------
INSERT INTO `sys_role_station` VALUES ('2', '100');
INSERT INTO `sys_role_station` VALUES ('2', '101');
INSERT INTO `sys_role_station` VALUES ('2', '105');
