/*
Navicat MySQL Data Transfer

Source Server         : dcjh
Source Server Version : 80011
Source Host           : localhost:3306
Source Database       : jh_db

Target Server Type    : MYSQL
Target Server Version : 80011
File Encoding         : 65001

Date: 2021-01-21 21:12:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sys_station`
-- ----------------------------
DROP TABLE IF EXISTS `sys_station`;
CREATE TABLE `sys_station` (
  `station_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '工站id',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父部门id',
  `ancestors` varchar(50) DEFAULT '' COMMENT '祖级列表',
  `station_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '设备名称',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `num` int(20) DEFAULT NULL COMMENT '鐠愮喕鐭楁禍?',
  `pic_url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '閼辨梻閮撮悽浣冪樈',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `status` char(1) DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`station_id`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8 COMMENT='部门表';

-- ----------------------------
-- Records of sys_station
-- ----------------------------
INSERT INTO `sys_station` VALUES ('100', '0', '0', '南方路机', '0', '1', '', 'c@qq.com', '0', '0', 'admin', '2021-01-13 23:44:06', 'admin', '2021-01-16 10:36:37');
INSERT INTO `sys_station` VALUES ('101', '100', '0,100', '工程搅拌', '1', '1', '/profile/upload/2021/01/20/d81573a1-5fba-46c6-b9a6-61f52b25745a.jpg', 'c@qq.com', '0', '0', 'admin', '2021-01-13 23:44:06', 'admin', '2021-01-16 10:36:19');
INSERT INTO `sys_station` VALUES ('102', '100', '0,100', '精品骨料加工处理', '2', '1', '/profile/upload/2021/01/20/269aa864-5170-4545-9238-49cfa9339b9d.jpg', 'c@qq.com', '0', '0', 'admin', '2021-01-13 23:44:06', 'admin', '2021-01-16 10:36:37');
INSERT INTO `sys_station` VALUES ('103', '100', '0,100,100', '再生骨料资源加工处理', '3', '1', '/profile/station/2021/01/14/71001043727276.jpg', 'c@qq.com', '0', '0', 'admin', '2021-01-13 23:44:06', '', null);
INSERT INTO `sys_station` VALUES ('104', '101', '0,100,101', '商品混凝土搅拌设备', '1', '2', '/profile/station/2021/01/14/126677578290797986g.jpg', 'c@qq.com', '0', '0', 'admin', '2021-01-13 23:44:06', '', null);
INSERT INTO `sys_station` VALUES ('105', '101', '0,100,101', '沥青混合料搅拌设备', '2', '2', '/profile/station/2021/01/14/126677578290797986g.jpg', 'c@qq.com', '0', '0', 'admin', '2021-01-13 23:44:06', '', null);
INSERT INTO `sys_station` VALUES ('106', '101', '0,100,101', '干混砂浆搅拌设备', '3', '1', '/profile/station/2021/01/14/126677578290797986g.jpg', 'c@qq.com', '0', '0', 'admin', '2021-01-13 23:44:06', '', null);
INSERT INTO `sys_station` VALUES ('107', '102', '0,100,102', '整形制砂设备', '1', '1', '/profile/station/2021/01/14/201908151251038343964.jpg', 'c@qq.com', '0', '0', 'admin', '2021-01-13 23:44:06', '', null);
INSERT INTO `sys_station` VALUES ('108', '102', '0,100,102', '移动破碎筛分设备', '2', '1', '/profile/station/2021/01/14/201908151251038343964.jpg', 'c@qq.com', '0', '0', 'admin', '2021-01-13 23:44:06', '', null);
INSERT INTO `sys_station` VALUES ('109', '102', '0,100,102', '固定破碎筛分设备', '3', '1', '/profile/station/2021/01/14/71001043727276.jpg', 'c@qq.com', '0', '0', 'admin', '2021-01-13 23:44:06', '', null);
INSERT INTO `sys_station` VALUES ('110', '103', '0,100,103', '废弃混凝土和砖瓦再生处理', '1', '1', '/profile/station/2021/01/14/71001043727276.jpg', 'c@qq.com', '0', '0', 'admin', '2021-01-13 23:44:06', '', null);
INSERT INTO `sys_station` VALUES ('111', '103', '0,100,103', '废弃余泥和渣土再生处理设备', '2', '1', '/profile/station/2021/01/14/71001043727276.jpg', 'c@qq.com', '0', '0', 'admin', '2021-01-13 23:44:06', '', null);
INSERT INTO `sys_station` VALUES ('112', '103', '0,100,103', '混合料剥离再生处理设备', '3', '1', '/profile/upload/2021/01/20/d81573a1-5fba-46c6-b9a6-61f52b25745a.jpg', 'c@qq.com', '0', '0', 'admin', '2021-01-21 15:44:55', '', null);
